import java.util.HashSet;
import java.util.Set;

public class BruteForce {
    private String plainText;
    private static final Set<String> COMMON_ENGLISH_WORDS = createCommonEnglishWordsSet();
    private static final Set<String> COMMON_RUSSIAN_WORDS = createCommonRussianWordsSet();


    // Maximum hard-coded offset
    private int maxOffset = 100000;
    private int currentOffset = 0;

    public int getCurrentOffset() {
        return currentOffset;
    }

    public BruteForce(String plainText) {
        this.plainText = plainText;
    }

    public String bruteForceDecrypt() {
        // create all possible variations starting with offset 1 and ending with 100000
        // split each offset by space symbols and compare retrieved words with common words sets
        // if we found matches, consider it as a potential key and complete

        // iterate offsets from 0, because there's a chance to get not ciphered test to decode
        for (int offset = 0; offset <= maxOffset; offset++) {

            // iterate decoding with offsets one by one
            String decodedText = decodeText(plainText, offset);

            // split each decoded text by words, using space symbol, "dot and space" and "comma and space"
            // remove punctuation marks from the array to better matching it with the common words sets
            String[] words = decodedText.split("\\s|,\\s|\\.\\s");
            for (int i = 0; i < words.length; i++) {
                words[i] = words[i].replaceAll("[^a-zA-Z0-9]", "");
            }

            // finding matches
            int englishWordMatches = countWordMatches(words, COMMON_ENGLISH_WORDS);
            int russianWordMatches = countWordMatches(words, COMMON_RUSSIAN_WORDS);

            // calculate amount of matches
            if (englishWordMatches + russianWordMatches > 0) {
                currentOffset = offset;
                return decodedText;
            }
        }
        return null; // If decryption is unsuccessful
    }

    private String decodeText(String cipheredText, int offset) {
        // change offset for each char in provided string

        StringBuilder decodedText = new StringBuilder();

        for (char c : cipheredText.toCharArray()) {
            char decodedChar = (char) (c - offset);
            decodedText.append(decodedChar);
        }

        return decodedText.toString();
    }

    private int countWordMatches(String[] words, Set<String> wordSet) {
        // count matches of each potentially decoded word with the provided common word set

        int matches = 0;

        for (String word : words) {
            if (wordSet.contains(word.toLowerCase())) {
                matches++;
            }
        }

        return matches;
    }

    private static Set<String> createCommonEnglishWordsSet() {
        // Create a set of common English words to compare against
        Set<String> commonWords = new HashSet<>();
        commonWords.add("the");
        commonWords.add("and");
        commonWords.add("is");
        commonWords.add("are");
        commonWords.add("but");
        // Add more common English words here to increase speed and accuracy

        return commonWords;
    }

    private static Set<String> createCommonRussianWordsSet() {
        // Create a set of common Russian words to compare against
        Set<String> commonWords = new HashSet<>();
        commonWords.add("и");
        commonWords.add("в");
        commonWords.add("на");
        commonWords.add("но");
        commonWords.add("с");
        commonWords.add("она");
        commonWords.add("мы");
        commonWords.add("вы");
        commonWords.add("они");
        commonWords.add("это");
        commonWords.add("один");
        commonWords.add("два");
        commonWords.add("себя");
        commonWords.add("мой");
        commonWords.add("наш");
        commonWords.add("свой");
        commonWords.add("сам");
        commonWords.add("что");
        commonWords.add("быть");
        commonWords.add("сказать");
        commonWords.add("говорить");
        commonWords.add("знать");
        commonWords.add("стать");
        commonWords.add("есть");
        commonWords.add("хотеть");
        commonWords.add("видеть");
        commonWords.add("идти");
        commonWords.add("стоять");
        commonWords.add("думать");
        commonWords.add("так");
        commonWords.add("там");
        commonWords.add("дать");
        commonWords.add("над");
        commonWords.add("при");
        commonWords.add("вот");
        commonWords.add("здесь");
        // Add more common words here to increase speed and accuracy

        return commonWords;
    }
}