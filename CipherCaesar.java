public class CipherCaesar extends Cipher{
    private String plainText;

    public CipherCaesar(String plaintext) {
        this.plainText = plaintext;
    }

    @Override
    public String encrypt(int offset) {
        //  It iterates through each character of the plaintext.
        //  For each letter, it calculates the new encrypted character using the ASCII code and the offset.

        StringBuilder encryptedText = new StringBuilder();

        for (char c : plainText.toCharArray()) {
            char encryptedChar = (char) (c + offset);
            encryptedText.append(encryptedChar);
        }

        return encryptedText.toString();
    }

    @Override
    public  String decrypt(int offset) {
        // decryption is just encryption with the inverse key
        return encrypt(-offset);
    }
}
