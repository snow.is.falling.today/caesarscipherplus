import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileManager {
    private String fileName;
    public FileManager(String filename) {
        this.fileName = filename;
    }

    public String readFile(){
        Path path = Paths.get(fileName);

        // Read all lines from the file
        byte[] fileBytes = new byte[1024];
        try {
            fileBytes = Files.readAllBytes(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Convert bytes to a string
        return new String(fileBytes);
    }

    public Path writeFile(String fileName, String content) {

        // Replace with the desired suffix
        String newSuffix = "_ciphered";

        try {
            // getting full path for the file
            Path originalPath = Paths.get(fileName).toAbsolutePath();
            //System.out.println(originalPath);

            // Get the parent directory and filename without extension
            Path parentDir = originalPath.getParent();
            String fileNameWithoutExtension = originalPath.getFileName().toString().replaceFirst("[.][^.]+$", "");

            // Append the suffix to the filename
            String newFileName = fileNameWithoutExtension + newSuffix + getExtension(originalPath);

            // Incremental counter for suffix if even file with the suffix above^^ exists
            int counter = 1;
            while (Files.exists(parentDir.resolve(newFileName))) {
                newFileName = fileNameWithoutExtension + newSuffix + counter + getExtension(originalPath);
                counter++;
            }

            // Create the new path with the updated filename
            Path newPath = parentDir.resolve(newFileName);

            // Rename the file to the new path
            Files.write(newPath, content.getBytes(StandardCharsets.UTF_8));
            return newPath;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private String getExtension(Path path) {
        // get a file extension, a letters after last '.' symbol, it it exists

        String fileName = path.getFileName().toString();
        int dotIndex = fileName.lastIndexOf('.');
        return (dotIndex == -1) ? "" : fileName.substring(dotIndex);
    }

    public static boolean validateFile(String fileName) {
        // check what provided filename is a file on a filesystem

        Path path = Paths.get(fileName);
        return Files.exists(path) && !Files.isDirectory(path);
    }
}
