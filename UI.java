import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;

public class UI {

    public void initialize() {
        // all interactions with user, getting a data from him, calling a functions to run a logic

        String action = "";
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {

            // Main menu dialog
            do {
                mainMenuDialog();
                action = reader.readLine();

                switch (action) {
                    case "1" -> followingEncodingDialog(reader,true);
                    case "2" -> followingEncodingDialog(reader,false);
                    case "3" -> bruteForceDialog(reader);
                    case "exit" -> System.out.println("Goodbye!");
                    default -> System.out.println("Invalid choice. Please try again.");
                }
            } while (!action.equalsIgnoreCase("exit"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void mainMenuDialog() {
        System.out.println("Choose an action:");
        System.out.println("1. Encode");
        System.out.println("2. Decode");
        System.out.println("3. Brute-force");
        System.out.print("Enter the number corresponding to your choice (or 'exit' to quit): ");
    }

    private void followingEncodingDialog(BufferedReader reader, boolean isEncoding) {
        // if isEncoding == true => encoding
        // if isEncoding == false => decoding

        try {
            System.out.print("Enter the filename: ");
            String inputFile = "";

            // getting a filename from console
            inputFile = reader.readLine();


            // check if entered string is file
            if (!FileManager.validateFile(inputFile)) {
                System.out.println("Invalid filename. Please try again.");
                return;
            }

            // while-loop for getting a correct KEY value
            int intValue = 0;
            while (true) {
                String input = "";
                System.out.print("Enter an Integer(1 - 100000) KEY offset (or isEncoding 'exit' to quit): ");

                // getting a key from console
                input = reader.readLine();


                // if entered 'exit' - exit to the main menu
                if (input.equalsIgnoreCase("exit")) {
                    System.out.println("Goodbye!");
                    return;
                }

                // check if entered key-value is integer and in (1 - 100000)
                if (isInteger(input)) {
                    intValue = Integer.parseInt(input);
                    System.out.println("Desired key is : " + intValue);
                    break;
                } else {
                    System.out.println("Input is not integer or not in the range.. Try again.");
                }
            }

            // read the file's data with the FileManager
            FileManager fileManager = new FileManager(inputFile);
            String data = fileManager.readFile();

            // push it to Caesar's object for encryption with key intValue
            CipherCaesar cipherCaesar = new CipherCaesar(data);

            // select provided isEncoding of process
            String newData = isEncoding ? cipherCaesar.encrypt(intValue) : cipherCaesar.decrypt(intValue);

            if (newData == null) {
                System.out.println("ciphering is unsuccessful, try again...");
                return;
            }

            // write the modified data to a new file
            Path newPath = fileManager.writeFile(inputFile, newData);
            System.out.println("New file has been created: " + newPath);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void bruteForceDialog(BufferedReader reader) {
        System.out.print("Enter the filename to perform brute-force on: ");
        String inputFile = null;
        try {
            inputFile = reader.readLine();
            if (!FileManager.validateFile(inputFile)) {
                System.out.println("Invalid filename. Please try again.");
                return;
            }

            // read the file's data with the FileManager
            FileManager fileManager = new FileManager(inputFile);
            String data = fileManager.readFile();

            // introduce a BF object and provide it file's data
            BruteForce bruteForce= new BruteForce(data);

            // start brute forcing
            String newData = bruteForce.bruteForceDecrypt();

            if (newData == null) {
                System.out.println("decryption is unsuccessful, try again...");
                return;
            }

            System.out.println("Potential key found!");
            System.out.println("Offset: " + bruteForce.getCurrentOffset());

            // write the modified data to a new file
            Path newPath = fileManager.writeFile(inputFile, newData);
            System.out.println("New file has been created: " + newPath);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean isInteger(String input) {
        // can be replaced by >return input.matches("^-?\\d+$");
        // but not necessary
        try {
            // trying to parse and check for hard-coded limitations
            int value = Integer.parseInt(input);
            return value >= 0 && value <= 100000;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
