import java.io.File;

public abstract class Cipher {
    public abstract String encrypt(int offset);

    public abstract String decrypt(int offset);

}
